#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"
#include <cstddef>
#include <variant>
#include <ptree.hpp>
#include <algorithm>

  
//std::cout is of type std::ostream, and nullptr is of type std::nullptr_t
std::ostream& operator << (std::ostream& os, std::nullptr_t ptr)
{
    return os << "nullptr"; 
}
struct CID {
    int id;
    friend bool operator==(const CID &lhs, const CID &rhs) {
        return lhs.id == rhs.id;
    }
};
using var = std::variant<int, std::string, CID, nullptr_t>;

//compilability check
template class miracle::ptree<CID>;
template class miracle::ptree_tree_iterator_policy<CID>;
template class miracle::ptree_child_iterator_policy<CID>;
template class miracle::ptree_generic_iterator<CID, miracle::ptree_tree_iterator_policy<CID>>;
template class miracle::ptree_generic_iterator<CID, miracle::ptree_child_iterator_policy<CID>>;


TEST_CASE("test property tree", "[ptree]") {
    
    miracle::ptree<var> p;
    REQUIRE( p.size() == 0 );
        
    SECTION("insert element via standart interface") {
        auto rit = p.insert("k1",1);
        REQUIRE(p.size() == 1);
        REQUIRE(rit.key() == "k1");
        REQUIRE(rit.value() == var(1));
    }
    SECTION("access to element via map and std interface") {
		auto rit = p.insert("k1", 1);
		auto a = p["k1"].key();
        auto b = p.at(0).key();
        REQUIRE( a == "k1" );
        REQUIRE( a == b );
        auto c = p["k1"].value();
        auto d = p.at(0).value();
        REQUIRE( c == var(1) );
        REQUIRE( c == d );
    }
    SECTION("insert element via map interface") {
        p["k2"] = 2;
        REQUIRE( p.size() == 1 );
        REQUIRE(p["k2"].key() == "k2");
        REQUIRE(p["k2"].value() == var(2));
    }
	SECTION("recursive access to element via map and std interface") {
		auto kit = p.insert("k1", 1);
        auto rit = kit.insert("r1", 2);
        auto a = p["k1"]["r1"].key();
        auto b = p.at(0).at(0).key();
        REQUIRE( a == "r1" );
        REQUIRE( a == b );
        auto c = p["k1"]["r1"].value();
        auto d = p.at(0).at(0).value();
        REQUIRE( c == var(2) );
        REQUIRE( c == d );
    }
    SECTION("recursive access to element via map path interface") {
		auto kit = p.insert("k1", 1);
        auto rit = kit.insert("r1", 2);
        auto a = p["k1/r1"].key();
        auto b = p.at(0).at(0).key();
        REQUIRE( a == "r1" );
        REQUIRE( a == b );
        auto c = p["k1/r1"].value();
        auto d = p.at(0).at(0).value();
        REQUIRE( c == var(2) );
        REQUIRE( c == d );
    }
    SECTION("recursive insert element via map interface") {
        p["k1"]["r1"] = 2;
        REQUIRE( p.size() == 1 );
        REQUIRE( p["k1"].size() == 1 );
        REQUIRE(p["k1"]["r1"].key() == "r1");
        REQUIRE(p["k1"]["r1"].value() == var(2));
    }
	SECTION("recursive insert element via map path interface") {
        p["k1/r1"] = 2;
        REQUIRE( p.size() == 1 );
        REQUIRE( p["k1"].size() == 1 );
        REQUIRE( p["k1/r1"].size() == 0 );
        REQUIRE(p["k1/r1"].key() == "r1");
        REQUIRE(p["k1/r1"].value() == var(2));
    }
    SECTION("compare node") {
        miracle::ptree<var> r("r",1);
        r["k1"] = 2;
        r["k1/r1"] = 3;
        miracle::ptree<var> u("r", 1);
        miracle::ptree<var> g("r", 1);
        g["k1"] = 2;
        g["k1/r1"] = 3;
        REQUIRE( r == r );
        REQUIRE( u == u );
        REQUIRE( r == u );
        REQUIRE( r == g );
    }
    SECTION("copy node") {
        miracle::ptree<var> r("r",1);
        r["k1"] = 2;
        r["k1/r1"] = 3;
        miracle::ptree<var> u("u", 5);
        REQUIRE( u.size() == 0 );
        
        u = r; // copy node 
        
        REQUIRE( u.size() == 1 );
        REQUIRE( u.key() == "r" );
        REQUIRE( u.value() == var(1) );
        REQUIRE( u["k1"].size() == 1 );
        REQUIRE( u["k1"].value() == var(2) );
        REQUIRE( u["k1/r1"].size() == 0 );
        REQUIRE( u["k1/r1"].value() == var(3));
        REQUIRE( r.size() == 1 );
        REQUIRE( r.key() == "r" );
        REQUIRE( r.value() == var(1) );
        REQUIRE( r["k1"].size() == 1 );
        REQUIRE( r["k1"].value() == var(2) );
        REQUIRE( r["k1/r1"].size() == 0 );
        REQUIRE( r["k1/r1"].value() == var(3));
    }
    
	SECTION("iterate elements via tree_iterator") {
		p["k1"] = 1;
		p["k2"] = 2;
		REQUIRE(p.size() == 2);
		auto it = p.tree_it();
		REQUIRE(it.key() == "");
		REQUIRE(it.value() == var());
		it++;
		REQUIRE(it.key() == "k1");
		REQUIRE(it.value() == var(1));
		it++;
		REQUIRE(it.key() == "k2");
		REQUIRE(it.value() == var(2));
		REQUIRE(it != it.end());
		it++;
		REQUIRE(it == it.end());
        
    }
    SECTION("std::find_if by element value") {
		p["k1"] = 1;
		p["k2"] = 2;
        auto it = p.find_if(p.begin(), p.end(),
            [](auto e) -> bool { return e.value() == var(2); });
        REQUIRE(it.value() == var(2));
    }
    SECTION("iterate empty tree") {
		REQUIRE(p.begin() == p.end());
        REQUIRE(p.tree_it().begin() != p.tree_it().end());     
    }
    SECTION("clear tree") {
        miracle::ptree<var> r("",3);
		r["k1"] = 1;
		r["k1/r1"] = 2;
        REQUIRE(r.size() == 1);
        r.clear();
		REQUIRE(r.size() == 0);
        REQUIRE(r.value() == var(3));     
    }
    SECTION("erase node") {
        miracle::ptree<var> r("",3);
		r["k1"] = 1;
		r["k1/r1"] = 2;
        auto it = r["k1/r1"].child_it();
        REQUIRE(r["k1"].size() == 1);
        bool fail = r.erase(it);
        REQUIRE(fail == false);
		REQUIRE(r["k1/r1"].value() == var(2));
        REQUIRE(r["k1"].value() == var(1));
        REQUIRE(r.value() == var(3));
        bool suc = r["k1"].erase(it);
        REQUIRE(suc == true);
		REQUIRE(r["k1"].size() == 0);
        REQUIRE(r["k1"].value() == var(1));
        REQUIRE(r.value() == var(3));
    }
    SECTION("insert iterator") {
        miracle::ptree<var> r("",1);
		r["k1"] = 2;
		r["k1/r1"] = 3;
        REQUIRE(r["k1"].size() == 1);
        miracle::ptree<var> t("",0);
		t["t1"] = 2;
        REQUIRE(t["t1"].size() == 0);
        t["t1"].insert(r["k1/r1"].disconnect());
        REQUIRE(t["t1"].size() == 1);
        REQUIRE(t["t1"].at(0).key() == "r1");
        REQUIRE(t["t1"].at(0).value() == var(3));
        REQUIRE(r["k1"].size() == 0);
        
    }
    SECTION("insert tree") {
        miracle::ptree<var> r("",1);
		r["k1"] = 2;
		r["k1/r1"] = 3;
        REQUIRE(r["k1"].size() == 1);
        miracle::ptree<var> t("",0);
		t["t1"] = 2;
        REQUIRE(t["t1"].size() == 0);
        //REQUIRE_THROWS( t["t1"].insert(r["k1"])); compile error. it is ok because explicit contstructor
        REQUIRE_THROWS( t["t1"].insert(&r["k1"])); //??
        auto it = r["k1"].disconnect();
        t["t1"].insert(it.node());
        
        REQUIRE(t["t1"].size() == 1);
        REQUIRE(t["t1"].at(0).key() == "k1");
        REQUIRE(t["t1"].at(0).value() == var(2));
        REQUIRE(t["t1/k1"].at(0).key() == "r1");
        REQUIRE(t["t1/k1"].at(0).value() == var(3));
        
        REQUIRE(r.size() == 0);
    }

    SECTION("test copy node") {
        auto vec = std::vector<CID>{CID{1},CID{2}};
        auto res = miracle::ptree<CID>();
        for (auto it = vec.begin(); it != vec.end(); it++) {
            CID e;
            e.id = (*it).id;
            res.insert("",e);
        }
        REQUIRE( res.size() == 2 );
        
        auto root = miracle::ptree<CID>("root", CID{3});
        auto entry = root;
        auto subroot = res;
        REQUIRE( subroot.size() == 2 );
        for (auto it = subroot.begin(); it != subroot.end(); ++it) {
            //auto x = &(it.node_ref());
             miracle::ptree<CID> x = it.node_ref(); // copy node 
            auto eit = entry.insert(x.child_it());
        }
        REQUIRE(entry.size() == 2 );
		REQUIRE(entry.value() == CID{3});
        REQUIRE(entry.at(0).value().id == CID{1}.id);
        REQUIRE(entry.at(1).value().id == CID{2}.id);
        
    }
    /* standart iterators not allow decrement
    SECTION("reverse iterate elements via tree_iterator") {
		p["k1"] = 1;
		p["k2"] = 2;
		auto it = p["k2"].tree_iterator();
		REQUIRE(it.key() == "k2");
		REQUIRE(it.value() == var(2));
        it--;
		REQUIRE(it.key() == "k1");
		REQUIRE(it.value() == var(1));
		it--;
		REQUIRE(it.key() == "");
		REQUIRE(it.value() == var());
		REQUIRE(it != it.end());
		it--;
		REQUIRE(it == it.end());
    }*/

}