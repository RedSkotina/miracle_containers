#ifndef MIRACLE_TREE_HEADER
#define MIRACLE_TREE_HEADER
#include <algorithm>
#include <exception>
#include <type_traits>
#include <vector>

namespace miracle {
// forward declaration
template <typename, typename> class generic_iterator;
template <typename> class tree_iterator_policy;
template <typename> class child_iterator_policy;

template <typename T> class tree {
  public:
    using tree_iterator_t = generic_iterator<T, tree_iterator_policy<T>>;
    using const_tree_iterator_t = const generic_iterator<T, tree_iterator_policy<T>>;
    using child_iterator_t = generic_iterator<T, child_iterator_policy<T>>;
    using const_child_iterator_t = const generic_iterator<T, child_iterator_policy<T>>;

  private:
    mutable T data_;
    mutable tree *parent_;
    std::vector<tree *> children_;

  public:
    // friend tree_iterator_t tree_iterator_policy<T>::next(const tree_iterator_t*) const;
    friend tree_iterator_t;
    friend child_iterator_t;
    friend tree_iterator_policy<T>;
    friend child_iterator_policy<T>;

    // friend typename tree<T>::child_iterator_t child_iterator<T>::next() const;

    // default constructor
    tree() {}
    // create tree with data in root
    tree(const T &data) : data_(data) {}
    // copy constructor
    tree(const tree &rhs) { *this = rhs; }
    // operator==, expects operator== has been written for both t and u
    const bool operator==(const tree &rhs) const {
        if (this->data_ == rhs.data_)
            return true;
        return false;
    }
    // The operator= which is a deep copy
    const tree &operator=(const tree &rhs) {
        this->clear();
        this->data_ = rhs.data_;
        this->deep_copy_tree(&rhs);
        return *this;
    }

    // destructor
    virtual ~tree() {}

    // deep copy tree
    void deep_copy_tree(const tree *rhs) {
        for (auto it = rhs->children_.begin(); it != rhs->children_.end(); it++) {
            auto rit = this->insert((*it)->data_);
            rit.node()->deep_copy_tree(*it);
        }
    }

    // Informative:

    // returns the number of children immediately contained within the tree
    size_t size() const { return this->children_->size(); }
    //
    bool isroot() const noexcept { return (this->parent == nullptr); }
    
    // Access:

    // returns a reference to the object contained within the node it is called on.
    T &operator*() { return this->data_; }
    // same as operator*().
    T &get() { return this->data_; }
    // same as operator*(), except const.
    const T &operator*() const { return this->data_; }
    // same as operator*() const.
    const T &get() const { return this->data_; }

    // Removal:

    // clears the current tree of all children within it.
    // It does not remove the data element associated with the tree that calls this
    // function.
    void clear() {
        for (auto it : children_)
            it->internal_clear();
        children_.clear();
    }

    // attempts to remove the element contained within the iterator from the tree's
    // children.
    // If the element is found and removed from the children's list from the tree, true is
    // returned. Otherwise false is returned. operator== is the function used to determine
    // equivalence in this function.
    template <typename T, typename U> bool erase(const generic_iterator<T, U> &it) {
        auto rit = std::find(children_.begin(), children_.end(), it.node());
        if (rit != children_.end()) {
            // delete rit children
            (*rit)->clear();
            // clear rit
            internal_delete((*rit)->data_);
            (*rit)->parent_ = nullptr;
            // delete rit
            children_.erase(rit);
            return true;
        }
        return false;
    }
    // Insertion:

    // inserts an element into the tree as a direct child of the current tree.
    // An iterator to the inserted child is returned.
    child_iterator_t insert(const T &data) {
        tree *newTree = new tree(data);
        newTree->parent_ = this;
        this->children_.push_back(newTree);
        return child_iterator_t(*newTree);
    }

    // inserts an element into the tree passing an iterator as the argument (the element
    // itself is what is inserted)
    // as a direct child of the current tree.
    // An iterator to the inserted child is returned.
    child_iterator_t insert(const child_iterator_t &it) { return insert(it.get()); }
    // inserts an existing tree into the current tree.
    // This works similar to the inserts of elements,
    // except that the entire tree already exists and all the tree's children go with the
    // insert.
    // An iterator to the inserted child is returned.
    child_iterator_t insert(tree *rhs) {
        rhs->parent_ = this;
        this->children_->push_back(rhs);
    }

    // Iteration:

    // Returns the first element of children
    child_iterator_t begin() const {
        return child_iterator_t(**(this->children_.begin()));
    }
    child_iterator_t begin() { return child_iterator_t(**(this->children_.begin())); }
    // Returns the first element of tree
    tree_iterator_t tree_begin() const {
        return tree_iterator_t(**(this->children_.begin()));
    }
    tree_iterator_t tree_begin() { return tree_iterator_t(**(this->children_.begin())); }

    // Returns end_of_iterator
    const child_iterator_t &end() const { return tree::child_iterator_t::end_iterator(); }
    const tree_iterator_t &tree_end() const {
        return tree::tree_iterator_t::end_iterator();
    }

    tree_iterator_t tree_iterator() const { return tree_iterator_t(*(this)); }
    tree_iterator_t tree_iterator() { return tree_iterator_t(*(this)); }
    child_iterator_t child_iterator() const { return child_iterator_t(*(this)); }
    child_iterator_t child_iterator() { return child_iterator_t(*(this)); }

  private:
    // End of the tree list, private only
    const tree *end_() const { return nullptr; }

    void internal_clear() {
        for (auto it : children_)
            it->internal_clear();
        children_.clear();
        internal_delete(data_);
        parent_ = nullptr;
    }
    void internal_delete(T data) {}
    void internal_delete(T *data) { delete data; }
};

// Generic Iterator template

template <typename T, typename policy> class generic_iterator {
  private:
    using TreeType = tree<T>;
    // friend typename tree<T>::tree_iterator_t tree_iterator_policy<T>::next(const
    // typename tree<T>::tree_iterator_t*) const;
    // friend typename tree<T>::child_iterator_t child_iterator_policy<T>::next(const
    // typename tree<T>::child_iterator_t*) const;
    friend child_iterator_policy<T>;
    friend tree_iterator_policy<T>;

    mutable TreeType *current_;

    static generic_iterator end_of_iterator;

    policy impl_;

  public:
    TreeType *operator&() = delete;
    const TreeType *operator&() const = delete;

    static const generic_iterator &end_iterator() { return end_of_iterator; }

    // Default constructor
    generic_iterator() : current_(nullptr) {}

    // Copy constructors for iterators
    generic_iterator(const generic_iterator &it) : current_(it.current_) {}

    // Copy constructor for trees
    generic_iterator(TreeType &rhs) : current_(&rhs) {}

    // Operator= for iterators
    const generic_iterator &operator=(const generic_iterator &it) const {
        this->current_ = it.current_;
        return (*this);
    }

    // Operator= for trees
    const generic_iterator &operator=(const TreeType &rhs) const {
        this->current_ = &(const_cast<TreeType &>(rhs));
        return (*this);
    }

    // Destructor
    ~generic_iterator(){};

    // Operator equals
    bool operator==(const generic_iterator &rhs) const {
        if (this->current_ == rhs.current_)
            return true;
        return false;
    }

    // Operator not equals
    bool operator!=(const generic_iterator &rhs) const { return !(*this == rhs); }

    // operator++, prefix
    const generic_iterator &operator++() const {
        this->current_ = this->next().current_;
        return (*this);
    }

    // operator++, postfix
    generic_iterator operator++(int)const {
        generic_iterator iTemp = *this;
        ++(*this);
        return (iTemp);
    }

    // operator--
    const generic_iterator &operator--() const {
        this->current_ = this->prev().current_;
        return (*this);
    }

    // Return the next guy
    generic_iterator next() const { return impl_.next(this); }

    // Return the prev guy
    generic_iterator prev() const { return impl_.prev(this); }

    // Insert into the iterator's tree
    generic_iterator insert(const T &t) { return this->current_->TreeType::insert(t); }

    generic_iterator insert(const generic_iterator &it) {
        return this->current_->TreeType::insert(it.current_);
    }

    generic_iterator begin() const { return impl_.begin(this); }
    generic_iterator begin() { return impl_.begin(this); }
    generic_iterator end() { return this->end_iterator(); }

    // get the data of the iter

    TreeType *node() const { return this->current_; }
    TreeType node_ref() const { return *(this->current_); }
    TreeType &operator*() { return *(this->current_); }
    const TreeType &operator*() const { return *(this->current_); }
    T &get() { return this->current_->get(); }
    const T &get() const { return this->current_->get(); }
};

template <typename T, typename policy>
generic_iterator<T, policy> generic_iterator<T, policy>::end_of_iterator;

template <typename T> class tree_iterator_policy {
  public:
    generic_iterator<T, tree_iterator_policy>
    next(const generic_iterator<T, tree_iterator_policy> *base) const {
        auto children = base->current_->children_;
        if (children.size() > 0) {
            return generic_iterator<T, tree_iterator_policy>(**(children.begin()));
        } else {
            auto parent = base->current_->parent_;
            auto cur_base = base->current_;
            while (parent) {
                auto parent_children = parent->children_;
                auto it =
                    std::find(parent_children.begin(), parent_children.end(), cur_base);
                if (it != parent_children.end()) {
                    it++;
                    if (it != parent_children.end())
                        return generic_iterator<T, tree_iterator_policy>(**it);
                    else {
                        cur_base = parent;
                        parent = parent->parent_;
                    }
                } else {
                    throw(std::exception("Cant find current_ in parent children"));
                }
            }
        }
        return base->end_iterator();
    }

    generic_iterator<T, tree_iterator_policy>
    prev(const generic_iterator<T, tree_iterator_policy> *base) const {
        /* BROKEN
        auto children = base->current_->children_;
        auto parent = base->current_->parent_;
        if (children.size() > 0) {
            return generic_iterator<T, tree_iterator_policy>(**(children.begin()));
        }
        else {

            while (parent) {
                auto parent_children = parent->children_;
                auto it = std::find(parent_children.begin(), parent_children.end(),
        base->current_);
                if (it != parent_children.end()) {
                    it--;
                    if (it != parent_children.end())
                        return generic_iterator<T, tree_iterator_policy>(**it);
                    else {
                        parent = parent->parent_;
                    }
                }
                else {
                    throw(std::exception("Cant find current_ in parent children"));
                }
            }
        }
        return base->end_iterator();
        */
    }

    generic_iterator<T, tree_iterator_policy>
    begin(const generic_iterator<T, tree_iterator_policy> *base) const {
        return generic_iterator<T, tree_iterator_policy>(*(base->current_));
    }
};

template <typename T> class child_iterator_policy {
  public:
    generic_iterator<T, child_iterator_policy>
    next(const generic_iterator<T, child_iterator_policy> *base) const {
        if (base->current_->parent_) {
            auto it = std::find(base->current_->parent_->children_.begin(),
                                base->current_->parent_->children_.end(), base->current_);
            if (it != base->current_->parent_->children_.end()) {
                it++;
                if (it != base->current_->parent_->children_.end())
                    return generic_iterator<T, child_iterator_policy>(**it);
            }
        }
        return base->end_iterator();
    }

    generic_iterator<T, child_iterator_policy>
    prev(const generic_iterator<T, child_iterator_policy> *base) const {
        if (base->current_->parent_) {
            auto it = std::find(base->current_->parent_->children_.begin(),
                                base->current_->parent_->children_.end(), base->current_);
            if (it != base->current_->parent_->children_.end()) {
                it--;
                if (it != base->current_->parent_->children_.end())
                    return generic_iterator<T, child_iterator_policy>(**it);
            }
        }
        return base->end_iterator();
    }

    generic_iterator<T, child_iterator_policy>
    begin(const generic_iterator<T, child_iterator_policy> *base) const {
        return base->current_->begin();
    }
};
}

#endif // MIRACLE_TREE_HEADER