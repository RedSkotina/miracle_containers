#ifndef MIRACLE_PTREE_HEADER
#define MIRACLE_PTREE_HEADER
#include <algorithm>
#include <exception>
#include <functional>
#include <string>
#include <type_traits>
#include <vector>

namespace miracle {

template <typename R, typename policy> class ptree_generic_iterator;
template <typename R> class ptree_tree_iterator_policy;
template <typename R> class ptree_child_iterator_policy;

template <typename T> class ptree {
  public:
    // forward declaration
    template <typename policy> using generic_iterator = ptree_generic_iterator<T, policy>;
    using tree_iterator_policy = ptree_tree_iterator_policy<T>;
    using child_iterator_policy = ptree_child_iterator_policy<T>;

    using tree_iterator_t = generic_iterator<tree_iterator_policy>;
    using const_tree_iterator_t = const generic_iterator<tree_iterator_policy>;
    using child_iterator_t = generic_iterator<child_iterator_policy>;
    using const_child_iterator_t = const generic_iterator<child_iterator_policy>;

  private:
    std::string key_;
    T data_;
    ptree *parent_;
    std::vector<ptree *> children_;

  public:
    friend tree_iterator_t;
    friend child_iterator_t;
    friend tree_iterator_policy;
    friend child_iterator_policy;

    // default constructor
    ptree() : parent_(nullptr) {}
    // create ptree with data in root
    ptree(const std::string &key, const T &data)
        : key_(key), data_(data), parent_(nullptr) {}
    // copy constructor
    ptree(const ptree &rhs) : parent_(nullptr) { *this = rhs; }
    // operator==, expects operator== has been written for both t and u
    // compare node without considering children
    const bool operator==(const ptree &rhs) const noexcept {
        return (this->data_ == rhs.data_);
    }
    // operator!=, expects operator== has been written for both t and u
    // compare node without considering children
    const bool operator!=(const ptree &rhs) const noexcept {
        return !(this->data_ == rhs.data_);
    }
    // The operator= which is a deep copy ptree except pointer to parent
    const ptree &operator=(const ptree &rhs) noexcept {
        this->clear();
        this->key_ = rhs.key_;
        this->data_ = rhs.data_;
        this->deepcopy(&rhs);
        return *this;
    }

    // destructor
    virtual ~ptree() {}

    // deep copy ptree
    void deepcopy(const ptree *rhs) noexcept {
        for (auto it = rhs->children_.begin(); it != rhs->children_.end(); it++) {
            auto rit = this->insert((*it)->key_, (*it)->data_);
            rit.node()->deepcopy(*it);
        }
    }

    const ptree &operator=(const T &data) noexcept {
        internal_delete(this->data_);
        this->data_ = data;
        return *this;
    }

    // Informative:

    // returns the number of children immediately contained within the ptree
    size_t size() const noexcept { return this->children_.size(); }
    //
    bool is_root() const noexcept { return (this->parent_ == nullptr); }

    // Element access::

    // returns the key of element
    const std::string &key() const noexcept { return this->key_; }
    // returns a reference to the object contained within the node it is called
    // on.
    T &value() { return this->data_; }
    // same as value(), const version.
    const T &value() const { return this->data_; }
    // same as operator*().
    T &operator*() { return this->data_; }
    // same as operator*(), except const.
    const T &operator*() const { return this->data_; }

    ptree *parent() { return this->parent_; }

    // returns a reference to the child identified with index
    // if the requested position is out of range throw an out_of_range exception.
    ptree &at(const size_t idx) const {
        if (idx >= this->children_.size())
            throw std::out_of_range("index out of range");
        auto rit = this->children_[idx];
        return *rit;
    }
    // returns a reference to the child identified with key
    // if key does not match the key of any child, the function throws an
    // out_of_range exception.
    ptree &at(const std::string &key) const {
        auto rit = std::find_if(children_.begin(), children_.end(),
                                [key](auto e) -> bool { return e->key_ == key; });
        if (rit != children_.end())
            return **rit;
        else
            throw std::out_of_range("childs not found");
    }
    // returns a reference to the first child identified with key
    // if key does not match the key of any child, , the function inserts a new
    // element with that key and returns a reference to child.
    ptree &operator[](const std::string &key) noexcept {
        auto sep_idx = key.find_first_of("/");
        std::string first_key = key.substr(0, sep_idx);
        std::string second_key;
        if (sep_idx != std::string::npos)
            second_key = key.substr(sep_idx + 1);

        auto rit =
            std::find_if(children_.begin(), children_.end(),
                         [first_key](auto e) -> bool { return e->key_ == first_key; });

        if (rit != children_.end())
            if (second_key != "")
                return (**rit)[second_key];
            else
                return **rit;
        else if (second_key != "")
            return (*(this->insert(first_key, T())))[second_key];
        else
            return *(this->insert(first_key, T()));
    }

    // Removal:

    // clears the current ptree of all children within it.
    // It does not remove the data element associated with the ptree that calls
    // this function.
    void clear() noexcept {
        for (auto it : children_)
            it->internal_clear();
        children_.clear();
    }

    // attempts to remove the element contained within the iterator from the
    // ptree's children.
    // If the element is found and removed from the children's list from the
    // ptree, true is returned. Otherwise false is returned. operator== is the
    // function used to determine equivalence in this function.
    template <typename T> bool erase(const generic_iterator<T> &it) noexcept {
        auto rit = std::find(children_.begin(), children_.end(), it.node());
        if (rit != children_.end()) {
            // delete rit children
            (*rit)->clear();
            // clear rit
            internal_delete((*rit)->data_);
            (*rit)->parent_ = nullptr;
            // delete rit
            children_.erase(rit);
            return true;
        }
        return false;
    }
    // Insertion:

    // inserts an element into the ptree as a direct child of the current ptree.
    // An iterator to the inserted child is returned.
    child_iterator_t insert(const std::string &key, const T &data) noexcept {
        ptree *new_tree = new ptree(key, data);
        new_tree->parent_ = this;
        this->children_.push_back(new_tree);
        return child_iterator_t(*new_tree);
    }

    // inserts an existing ptree from iterator into the current ptree as a direct child
    // An iterator to the inserted child is returned.
    child_iterator_t insert(child_iterator_t &it) {
        return insert(it.node());
    }
    // inserts an existing ptree into the current ptree.
    // This works similar to the inserts of elements,
    // except that the entire ptree already exists and all the ptree's children go
    // with the insert.
    // rhs ptree must be disconnected before.
    // An iterator to the inserted child is returned.
    child_iterator_t insert(ptree *rhs) {
        if (rhs->parent_ != nullptr)
            throw std::exception("ptree* rhs is not disconnected");
		ptree *new_tree = new ptree(*rhs);
		new_tree->parent_ = this;
        this->children_.push_back(new_tree);
        return child_iterator_t(*new_tree);
    }

    // Disconnect node from parrent node.
    // Remove self from parent children_.
    // An iterator to self is returned.
    child_iterator_t disconnect() {
        if (this->parent_) {
            auto it = std::find(this->parent_->children_.begin(),
                                this->parent_->children_.end(), this);
            if (it != this->parent_->children_.end())
                this->parent_->children_.erase(it);
            else
                throw std::exception("cant found self in parent children");
            this->parent_ = nullptr;
        }
        return child_iterator_t(*this);
    }
    // Find:
    template <typename T>
    child_iterator_t find_if(const generic_iterator<T> &it1,
                             const generic_iterator<T> &it2,
                             std::function<bool(const ptree &)> pred) {
        for (auto it = it1; it != it2; it++)
            if (pred(*it))
                return it;
        return end();
    }
    // Iteration:

    // Returns the first element of children
    child_iterator_t begin() const {
        if (this->children_.empty())
            return end();
        return child_iterator_t(**(this->children_.begin()));
    }
    child_iterator_t begin() {
        if (this->children_.empty())
            return end();
        return child_iterator_t(**(this->children_.begin()));
    }
    // Returns the first element of ptree
    // tree_iterator_t tree_begin() const { return tree_iterator_t(*this); }
    tree_iterator_t tree_begin() { return tree_iterator_t(*this); }

    // Returns end_of_iterator
    const child_iterator_t &end() const {
        return ptree::child_iterator_t::end_iterator();
    }
    const tree_iterator_t &tree_end() const {
        return ptree::tree_iterator_t::end_iterator();
    }

    // tree_iterator_t tree_iterator() const { return tree_iterator_t(*(this)); }
    tree_iterator_t tree_it() { return tree_iterator_t(*(this)); }
    // child_iterator_t child_iterator() const { return child_iterator_t(*(this)); }
    child_iterator_t child_it() { return child_iterator_t(*(this)); }

  private:
    // End of the ptree list, private only
    const ptree *end_() const noexcept { return nullptr; }

    void internal_clear() noexcept {
        for (auto it : children_)
            it->internal_clear();
        children_.clear();
        internal_delete(data_);
        parent_ = nullptr;
    }
    void internal_delete(T data) noexcept {}
    void internal_delete(T *data) noexcept { delete data; }
};

template <typename R, typename policy> class ptree_generic_iterator {
  private:
    using TreeType = ptree<R>;
    friend ptree_child_iterator_policy<R>;
    friend ptree_tree_iterator_policy<R>;

    mutable TreeType *current_;

    static ptree_generic_iterator end_of_iterator;

    policy impl_;

  public:
    // TreeType *operator&() = delete;
    // const TreeType *operator&() const = delete;

    static const ptree_generic_iterator &end_iterator() noexcept {
        return end_of_iterator;
    }

    // Default constructor
    ptree_generic_iterator() : current_(nullptr) {}

    // Copy constructors for iterators
    ptree_generic_iterator(const ptree_generic_iterator &it) : current_(it.current_) {}

    // Copy constructor for trees
    explicit
    ptree_generic_iterator(TreeType &rhs)  : current_(&rhs) {}

    // Operator= for iterators
    const ptree_generic_iterator &operator=(const ptree_generic_iterator &it) const
        noexcept {
        this->current_ = it.current_;
        return (*this);
    }
    /*
    // Operator= for trees
    const generic_iterator &operator=(const TreeType &rhs) noexcept {
        this->current_ = &(rhs);
        return (*this);
    }
    */
    // Destructor
    ~ptree_generic_iterator(){};

    // Operator equals
    bool operator==(const ptree_generic_iterator &rhs) const noexcept {
        if (this->current_ == rhs.current_)
            return true;
        return false;
    }

    // Operator not equals
    bool operator!=(const ptree_generic_iterator &rhs) const noexcept {
        return !(*this == rhs);
    }

    // operator++, prefix
    const ptree_generic_iterator &operator++() noexcept {
        this->current_ = this->next().current_;
        return (*this);
    }

    // operator++, postfix
    ptree_generic_iterator operator++(int)noexcept {
        ptree_generic_iterator iTemp = *this;
        ++(*this);
        return (iTemp);
    }

    // operator--
    const ptree_generic_iterator &operator--() noexcept {
        this->current_ = this->prev().current_;
        return (*this);
    }
    // operator--, postfix
    ptree_generic_iterator operator--(int)noexcept {
        ptree_generic_iterator iTemp = *this;
        --(*this);
        return (iTemp);
    }
    // Return the next guy
    ptree_generic_iterator next() const { return impl_.next(this); }

    // Return the prev guy
    ptree_generic_iterator prev() const { return impl_.prev(this); }

    // Insert into the iterator's ptree
    ptree_generic_iterator insert(const std::string &key, const R &t) noexcept {
        return ptree_generic_iterator(
            this->current_->TreeType::insert(key, t).node_ref());
    }

    ptree_generic_iterator insert(const ptree_generic_iterator &it) noexcept {
        return ptree_generic_iterator(
            this->current_->TreeType::insert(it.current_).node_ref());
    }

    ptree_generic_iterator begin() const { return impl_.begin(this); }
    // ptree_generic_iterator begin() { return impl_.begin(this); }
    const ptree_generic_iterator end() const { return this->end_iterator(); }

    // returns the number of children immediately contained within the ptree
    size_t size() const noexcept { return impl_.size(this); }

    // get the data of the iter

    TreeType *node() const noexcept { return this->current_; }
    TreeType &node_ref() const noexcept { return *(this->current_); }
    // TreeType& operator*() const noexcept { return *(this->current_); }
    TreeType &operator*() const noexcept { return *(this->current_); }
    const std::string &key() const noexcept { return this->current_->key(); }
    // T& value() { return this->current_->value(); }
    const R &value() const noexcept { return this->current_->value(); }
    R &value() noexcept { return this->current_->value(); }
};

template <typename R> class ptree_tree_iterator_policy {
  public:
    ptree_generic_iterator<R, ptree_tree_iterator_policy>
    next(const ptree_generic_iterator<R, ptree_tree_iterator_policy> *base) const {
        auto children = base->current_->children_;
        if (children.size() > 0) {
            return ptree_generic_iterator<R, ptree_tree_iterator_policy>(
                **(children.begin()));
        } else {
            auto parent = base->current_->parent_;
            auto cur_node = base->current_;
            while (parent) {
                auto parent_children = parent->children_;
                auto it =
                    std::find(parent_children.begin(), parent_children.end(), cur_node);
                if (it != parent_children.end()) {
                    it++;
                    if (it != parent_children.end())
                        return ptree_generic_iterator<R, ptree_tree_iterator_policy>(
                            **it);
                    else {
                        cur_node = parent;
                        parent = parent->parent_;
                    }
                } else {
                    throw(std::exception("Cant find current_ in parent children"));
                }
            }
        }
        return base->end_iterator();
    }

    ptree_generic_iterator<R, ptree_tree_iterator_policy>
    prev(const ptree_generic_iterator<R, ptree_tree_iterator_policy> *base) const {
        auto parent = base->current_->parent_;
        auto cur_node = base->current_;

        if (parent) {
            auto parent_children = parent->children_;
            auto it = std::find(parent_children.begin(), parent_children.end(), cur_node);
            if (it != parent_children.end()) {
                it--;
                if (it != parent_children.end()) {
                    auto cit = (*it);
                    while (cit->children_.size() > 0)
                        cit = *(cit->children_.rbegin());
                    return ptree_generic_iterator<R, ptree_tree_iterator_policy>(*cit);
                } else {
                    return ptree_generic_iterator<R, ptree_tree_iterator_policy>(*parent);
                }
            } else {
                throw(std::exception("Cant find current_ in parent children"));
            }
        }
        return base->end_iterator();
    }

    ptree_generic_iterator<R, ptree_tree_iterator_policy>
    begin(const ptree_generic_iterator<R, ptree_tree_iterator_policy> *base) const {
        return ptree_generic_iterator<R, ptree_tree_iterator_policy>(*(base->current_));
    }

    size_t size(const ptree_generic_iterator<R, ptree_tree_iterator_policy> *base) const {
        return base->current_->size();
    }
};

template <typename R> class ptree_child_iterator_policy {
  public:
    ptree_generic_iterator<R, ptree_child_iterator_policy>
    next(const ptree_generic_iterator<R, ptree_child_iterator_policy> *base) const {
        if (base->current_->parent_) {
            auto it = std::find(base->current_->parent_->children_.begin(),
                                base->current_->parent_->children_.end(), base->current_);
            if (it != base->current_->parent_->children_.end()) {
                it++;
                if (it != base->current_->parent_->children_.end())
                    return ptree_generic_iterator<R, ptree_child_iterator_policy>(**it);
            }
        }
        return base->end_iterator();
    }

    ptree_generic_iterator<R, ptree_child_iterator_policy>
    prev(const ptree_generic_iterator<R, ptree_child_iterator_policy> *base) const {
        if (base->current_->parent_) {
            auto it = std::find(base->current_->parent_->children_.begin(),
                                base->current_->parent_->children_.end(), base->current_);
            if (it != base->current_->parent_->children_.end()) {
                it--;
                if (it != base->current_->parent_->children_.end())
                    return ptree_generic_iterator<R, ptree_child_iterator_policy>(**it);
            }
        }
        return base->end_iterator();
    }

    ptree_generic_iterator<R, ptree_child_iterator_policy>
    begin(const ptree_generic_iterator<R, ptree_child_iterator_policy> *base) const {
        return base->current_->begin();
    }

    size_t
    size(const ptree_generic_iterator<R, ptree_child_iterator_policy> *base) const {
        return base->current_->size();
    }
};

template <typename R, typename policy>
ptree_generic_iterator<R, policy> ptree_generic_iterator<R, policy>::end_of_iterator;

} // namespace miracle

#endif // MIRACLE_PTREE_HEADER